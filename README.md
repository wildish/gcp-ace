[[_TOC_]]

# Examples and demos for the GCP ACE certification

This repository holds example scripts and demos for the Google Associate Cloud Engineer certification Coursera specialisation. The idea is to build it into a set of examples that can be used to accelerate learning for future cohorts, as well as serving as a reference for anyone who has taken the courses.

If you want to contribute to it, please fork the repository and make pull requests for any changes you want to add.

If you find any problems with the code here, please open an issue.

# Getting started

We have a lot of documentation already in place, or being built, for people taking the GCP ACE specialisation at EBI. We have a [README](https://bitly.com/ebi-cloud-certification-readme]) describing our organisation, and a [FAQ](https://bit.ly/ebi-gcp-ace-faq]) that is emerging from our discussions.

# Using cloud shell or gcloud

See the [*demos/gcloud*](demos/gcloud) subdirectory for some examples. Please feel free to contribute your own!