#!/bin/bash

cd `dirname $0`
which rsync
source ./env.sh

echo /usr/bin/rsync \
  --rsh ./ssh.sh \
  --recursive \
  --partial \
  --times \
  --perms \
  --links \
  --hard-links \
  --one-file-system \
  --progress $host:download/ . 
