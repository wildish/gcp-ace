#
# The billing account is the only one we're allowed to use with our EBI accounts.
billing_account="00F8FC-525826-16C3A7" # Main EBI Billing Account

#
# The folder is the location in the GCP  EBI organisation tree. Get it from the
# GCP console -> IAM & Admin -> Manage Resources view
folder=187759886721 # ebi.ac.uk -> Technical Services -> TSI -> Cloud Certification
#
# @Gift, this is for you...
# folder=122124219091 # ebi.ac.uk -> Service Teams -> Protein Sequence Resources

#
# Choose a name for your project. Here I just build a generic name that is likely
# to be unique by adding the current date to it.
project_name_file=".projectname"
if [ -f $project_name_file ]; then
  project=`cat $project_name_file`
else
  timestamp=`date +%g-%m-%d`
  # timestamp=`date +%g-%m-%d-%H:%M` # If I want higher timestamp resolution...
  project="$USER-slurm-$timestamp"
  echo $project > $project_name_file
fi

#
# Set a region and zone. Choose London unless you have a good reason not to, it's best
# that we stay within UK jurisdiction if we can.
region="europe-west2"
zone="${region}-a"

#
# This is the name of the cluster to create
cluster_name="ips2"
