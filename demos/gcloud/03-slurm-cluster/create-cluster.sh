#!/bin/sh

. ./env.sh
config_file="${cluster_name}.yaml"
controller_node="${cluster_name}-controller"

#
# Sanity checks...
if [ "$cluster_name" == "" ]; then
  echo "Expected to find \$cluster_name set, but it's not"
  exit 1
fi

if [ ! -f $config_file ]; then
  echo "Expected to find a deployment configuration file, $config_file, but it's not there"
  exit 1
fi

dir="slurm-gcp"
if [ ! -d $dir ]; then
  git clone https://github.com/SchedMD/slurm-gcp.git
  [ -f $dir/cluster.yaml ] && mv $dir/cluster.yaml{,.orig}
fi
cat $config_file | \
  sed \
    -e "s/CLUSTER_NAME/$cluster_name/" \
    -e "s/ZONE/$zone/" | \
  tee $dir/$config_file >/dev/null
cd $dir

gcloud config configurations activate $project && \
gcloud deployment-manager deployments create $cluster_name --config $config_file && \
gcloud compute scp ../test-slurm.sh ${controller_node}: