# This demo shows one way to manage gcloud projects.

Gcloud projects should be thought of as being as disposable as other resources, like virtual machines, and to use each project for one thing and one thing only. Google has [more about creating and deleting projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects) on their website.

Given you can have multiple projects, you need to be able to control which one you're managing or interacting with at any time. The easy way to do that is to create a [**gcloud configuration**](https://cloud.google.com/sdk/gcloud/reference/config/configurations) for each project, and simply switch from one project to the next by switching configurations. A configuration is simply a way of grouping a set of default values together, such as the project ID, the default region for that project, and so on. That can greatly simplify the number of options you have to put in when running gcloud commands, and help avoid mistakes such as using the wrong region etc.

This demo shows how to create a project with the **gcloud** CLI, how to create a configuration for it, and how to use that configuration when creating a virtual machine. Be sure to read the scripts themselves, they contain more detail than is captured here.

**Tip:** Create a configuration called **default**, set the project, region and zone to **none**. Then you can activate the default configuration and be sure you are not messing with a real project!

## Creating a project
The **create-project.sh** script does what it says, it creates a project. It uses **env.sh** to source the parameters it will need. In this example, I use **env.sh** to build a project name including the current date, then save it in a file, **.projectname**. Next time I come to source the **env.sh** script it picks the name up from there instead.

The **create-project.sh** script will check if the project already exists, and only create it if it doesn't exist already. Then it sets the default zone, region and account, enables a few of the more common APIs, and just for fun, it sets up a couple of firewall rules. Customise this as you wish.

## Creating a virtual machine
The **create-vm.sh** script does this, again using the **env.sh** script to get its parameters. It assumes the project has already been created, and will fail spectacularly if it hasn't. It assumes that the **host**, **instance** and **disk_size** variables have been set to the desired hostname, the type of machine to create, and the size of the boot disk, respectively.

## Accessing the machine
Once your machine is running, you'll want to log in to it. Just run the **ssh.sh** script for that, and you'll be logged in automatically.

One interesting feature, if you create the VM with the **--no-address** flag, it won't have a public internet address, so is not visible on the internet. The **ssh.sh** script will _still_ succeed, because it will detect this, and go through the Google **Identity Aware Proxy** (IAP) tunnel, logging you in as normal. That's a rather neat way of putting up a machine and protecting it from undesired login attempts!

## Deleting the VM or project
You can use the **delete-vm.sh** script to delete the VM, and the **delete-project.sh** script to delete the project. You don't _have_ to delete the VM first, but it's good practice. A deleted project remains in the 'recycle bin' for 30 days, and can be recalled at any time during that period. Deleting the VM up front is just tidier, in my opinion.