#!/bin/bash

cd `dirname $0`

#
# Source the environment script to pick up the variable definitions.
. ./env.sh

#
# First delete the project itself
gcloud config configurations activate $project
gcloud projects delete $project

#
# Then delete the configuration too. For that, you have to be in a
# different configuration, you can't delete the active configuration.
gcloud config configurations activate default
gcloud config configurations delete $project
