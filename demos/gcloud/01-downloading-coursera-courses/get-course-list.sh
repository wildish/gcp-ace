#!/bin/bash

start=0
step=500
while [ $start -lt 5800 ]
do
  echo $start

  file=$(printf "course-list-%04d.json" $start)
  echo "File: $file"
  
  if [ ! -f $file ]; then
    curl -o $file "https://api.coursera.org/api/courses.v1?start=$start&limit=$step"
  fi

  start=$[$start + $step]
done

(
  echo "Slug,Name"

  for file in course-list*.json
  do
    length=$(cat $file | jq ".elements | length")
    index=0
    while [ $index -lt $length ]
    do
      slug=$(cat $file | jq ".elements[$index] | .slug" | tr -d '"' )
      name=$(cat $file | jq ".elements[$index] | .name" | tr -d '"' )
      echo "$slug,\"$name\""
      index=$[$index + 1]
    done
  done
) | tee list-of-courses.txt
