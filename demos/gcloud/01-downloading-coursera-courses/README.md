# This shows how to use a VM in GCP to download your Coursera courses!

## Getting started
If you want to use a Google VM for this, start by copying the scripts from **../00-managing-projects** here, configure the **env.sh** if you want to customise it, and create a project and a virtual machine with the **create-project.sh** and **create-vm.sh** scripts.

SSH into the machine:
```
./ssh.sh
```

then install the coursera-dl tool. You get the base tool first, then install a patch to fix a bug:
```
sudo yum install -y patch
pip2 install --user coursera-dl
cat << EOF >patchfile
--- .local/lib/python2.7/site-packages/coursera/coursera_dl.py  2020-10-07 12:43:43.614061745 +0000
+++ coursera-dl/coursera/coursera_dl.py 2020-10-07 12:39:17.041614281 +0000
@@ -105,7 +105,11 @@
     @type args: namedtuple
     """
     session = get_session()
-    login(session, args.username, args.password)
+    if args.cookies_cauth:
+        session.cookies.set('CAUTH', args.cookies_cauth)
+    else:
+        login(session, args.username, args.password)
+
     extractor = CourseraExtractor(session)
     courses = extractor.list_courses()
     logging.info('Found %d courses', len(courses))
EOF
patch .local/lib/python2.7/site-packages/coursera/coursera_dl.py patchfile 

```

Alternatively, you can install coursera-dl on your own laptop. It's up to you.

## Authenticating with coursera-dl
The tool currently seems to be buggy, the **-u** and **-p** options don't work well. The only way I found that works is to grab the **CAUTH** cookie from your browser and use that instead.

For Safari, go to the **Developer** -> **Show Web Inspector** menu item. Select the **Storage** tab, then **Cookies** on the left hand side.

Find the CAUTH cookie, copy it to a text file, and extract just the long cookie string, not all the cruft that comes with it.

If you're using another browser, please google how to do it for your browser, and provide instructions here.

In your VM, set an environment variable to hold the cookie:
```export cauth='very-long-cookie-string'```

**N.B.** Don't save the cookie to a file on your VM. That would effectively give your account credentials to anyone who can log into the VM, which isn't good

## Downloading an entire specialisation or professional certificate
The hard part here is getting the certification 'slug'. The 'slug' is the part of the URL in the browser that identifies the course or specialisation/professional certification. The only way I've found is to go into the web page for the specialisation and then grub around in the developer view in Safari to pull it out of the data files.

Specifically, look at the 'Sources' view, under www.coursera.org -> Fetches, then look in the graphqlBatch files (probably the second to last file). From the top, you'll see a json object data -> XdpV1Resource -> get -> xdpMetadata -> sdpMetadata -> slug. That's the magic word!

There's another way to get course information, and that's to use the Coursera API. That's documented, somewhat, at https://build.coursera.org/app-platform/catalog. Or you can reverse-engineer the coursera-dl code to figure it out. That leads to scripts like **get-course-list.sh** and **get-specialisation-list.sh**. You can investigate those for yourself if you want.

Anyway, for the Associate Cloud Engineer, the slug is **cloud-engineering-gcp**. For the Professional Cloud Architect, it's **gcp-cloud-architect**.

Then, download the specialisation:
```
coursera-dl --cauth $cauth \
  --subtitle-language en \
  --download-notebooks \
  --specialization \
  --video-resolution 720p \
  --path download/$slug \
  $slug
```

This will download the specialisation into the **download/$slug** directory. Note that where specialisations share courses, you will inevitably download one copy of the course per specialisation.

## Downloading individual courses
### Finding the course 'slug'
To find the course slug, simply go into the course in the browser and look at the URL. For example, in the second course in the specialisation, **Essential Google Cloud Infrastructure: Foundation**, the URL is **https://www.coursera.org/learn/gcp-infrastructure-foundation/home/welcome**. The slug is the part that identifies the course, so **gcp-infrastructure-foundation**.

Or, easier, with the patch applied when you installed the code, you can use the **--list-courses** argument:

```


## Downloading the courses one by one
For an individual course, just run this command:
```
coursera-dl --cauth $cauth --path download --subtitle-language en $slug
```

_et voila_, the course material appears magically under the **download* subdirectory!

Here's a list of the slugs for the ACE specialisation (N.B. This is missing the K8s course that was added)

Course title | slug
-------------|-----
Google Cloud Platform Fundamentals: Core Infrastructure | gcp-fundamentals
Essential Google Cloud Infrastructure: Foundation | gcp-infrastructure-foundation
Essential Google Cloud Infrastructure: Core Services | gcp-infrastructure-core-services
Elastic Google Cloud Infrastructure: Scaling and Automation | gcp-infrastructure-scaling-automation
Preparing for the Google Cloud Associate Cloud Engineer Exam | preparing-cloud-associate-cloud-engineer-exam

So to get the lot, just copy and paste this code snippet:

```
for slug in \
  gcp-fundamentals \
  gcp-infrastructure-foundation \
  gcp-infrastructure-core-services \
  gcp-infrastructure-scaling-automation \
  preparing-cloud-associate-cloud-engineer-exam
do
  coursera-dl --cauth $cauth --subtitle-language en --download-notebooks --video-resolution 720p --path download  $slug
done
```

The whole thing comes to about 1.7 GB.


If you want the extra two courses for the Professional Cloud Architect course too, they are:

Course title | slug
-------------|-----
Reliable Google Cloud Infrastructure: Design and Process | cloud-infrastructure-design-process
Preparing for the Google Cloud Professional Cloud Architect Exam | preparing-cloud-professional-cloud-architect-exam

## Syncing the files back to your laptop
How do you get the files back to your laptop? Take a look at the next code example, **../02-synchronising-data**!
