#!/bin/bash

for spec in \
  cloud-engineering-gcp \
  gcp-cloud-architect
do
  [ -d download/$spec ] && continue
  echo $spec
  coursera-dl --cauth $cauth \
    --subtitle-language en \
    --download-notebooks \
    --specialization \
    --video-resolution 720p \
    --path {download/,}$spec
  echo "Sleeping..."
done
