#!/bin/bash

echo "Warning: This doesn't work properly, but I haven't debugged it"
exit 0

ls -1 download/* | egrep -v ':|^$' |  tee a | sort | uniq | tee b >/dev/null
for course in `cat b`
do
  echo $course `grep -c $course a`
done | tee c | egrep -v ' 1' | awk '{ print $1 }' | tee d

for course in `cat d`
do
  echo ' '
  reference=""
  for d in download/*/$course
  do
    if [ -d $d ]; then
      if [ -L $d ]; then
        echo "Skip $d (symbolic link)"
      else
        if [ "$reference" != "" ]; then
          echo "Prune $d"
          if [ "$reference" == "$d" ]; then
            echo "Whoa, about to shoot myself in the foot"
            exit 0
          fi
          ( cd `dirname $d`; pwd; ls -l `basename $d` )
          # rm -rf $d
          # ln -s $d $reference
        else
          reference=$d
          echo "Reference = $d"
        fi
      fi
    else
      echo "$d is not a directory"
    fi
  done
done
